package block

import (
	"encoding/hex"
	"errors"
	"time"

	"gitlab.com/notdodo/dodollar/pow"
	"golang.org/x/crypto/sha3"
)

// block `struct` represents a block in the blockchain
//	`index` is the position of the data record in the blockchain
//  `timestamp` is automatically determined and is the time the data is written
//  `data` or beats per minute, is your pulse rate
//  `hash` is a SHA512 identifier representing this data record
//  `prevHash` is the SHA512 identifier of the previous record in the chain
type Block struct {
	Index      int
	Timestamp  string
	Data       []byte
	Hash       string
	HeaderHash string
	PrevHash   string
	Nonce      int64
}

type Blockchain struct {
	Blocks []*Block
	Length int
}

func NewBlockchain() *Blockchain {
	genesisBlock := Block{0, time.Now().UTC().String(), []byte("Genesis Block"), "", "", "", 0}
	genesisBlock.HeaderHash = genesisBlock.calculateHeader()
	pow := pow.NewProofOfWork(genesisBlock.Index, genesisBlock.Data, genesisBlock.HeaderHash)
	nonce, hash := pow.Run()
	genesisBlock.Nonce = nonce
	genesisBlock.Hash = hash
	if pow.Validate(nonce) {
		return &Blockchain{[]*Block{&genesisBlock}, 1}
	}
	return &Blockchain{[]*Block{}, 0}
}

func (b *Blockchain) getLast() *Block {
	return b.Blocks[len(b.Blocks)-1]
}

func (b *Blockchain) GetBlocks() ([]*Block, error) {
	if len(b.Blocks) <= 0 {
		return nil, errors.New("block: blockchain is empty")
	}
	return b.Blocks, nil
}

// `calculateHash` function concatenates `index`, `timestamp` and `prevHash` of a block
// returns the SHA512 hash of this block
func (b *Block) calculateHeader() string {
	hash := make([]byte, 32)
	record := string(rune(b.Index)) + b.Timestamp + b.PrevHash
	sha3.ShakeSum256(hash, []byte(record))
	return hex.EncodeToString(hash)
}

// `generateBlock` generate a new block using the new `data` information and calculate its
// hash e will be chained to the previous
func (bc *Blockchain) GenerateNextBlock(data []byte) (interface{}, error) {
	var newBlock Block
	lastBlock := bc.getLast()
	newBlock.Index = lastBlock.Index + 1
	newBlock.Timestamp = time.Now().UTC().String()
	newBlock.Data = []byte(data)
	newBlock.PrevHash = lastBlock.Hash
	newBlock.HeaderHash = newBlock.calculateHeader()

	pow := pow.NewProofOfWork(newBlock.Index, newBlock.Data, newBlock.HeaderHash)
	nonce, hash := pow.Run()
	newBlock.Nonce = nonce
	newBlock.Hash = hash

	if lastBlock.isBlockValid(newBlock, pow) {
		newBlocks := append(bc.Blocks, &newBlock)
		newBlockchain := &Blockchain{newBlocks, len(newBlocks)}
		if bc.replaceChain(newBlockchain.Blocks) {
			return newBlock, nil
		}
		return nil, errors.New("block: valid but not in the longest branch")
	}
	return nil, errors.New("block: is not valid")
}

// `isBlockValid` checks is a block is correctly the one to be added to the chain
func (b *Block) isBlockValid(newBlock Block, pow *pow.ProofOfWork) bool {
	layout := "2006-01-02 15:04:05.999999999 -0700 UTC"
	actualT, _ := time.Parse(layout, newBlock.Timestamp)
	prevT, _ := time.Parse(layout, b.Timestamp)
	duration := (time.Since(prevT) - time.Since(actualT)).Minutes()
	// check index, prevHash, HeaderHash, timestamp and PoW
	return b.Index+1 == newBlock.Index &&
		b.Hash == newBlock.PrevHash &&
		newBlock.HeaderHash == newBlock.calculateHeader() &&
		actualT.After(prevT) && duration < 120 &&
		pow.Validate(newBlock.Nonce)
}

// `replaceChain` make sure the chain is longer than the current blockchain
func (b *Blockchain) replaceChain(newBlocks []*Block) bool {
	if len(newBlocks) > b.Length {
		b.Blocks = newBlocks
		b.Length = len(newBlocks)
		return true
	}
	return false
}

func (b *Blockchain) Validate() error {
	return nil
}
