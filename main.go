package main

import (
	"log"

	"github.com/joho/godotenv"
	"gitlab.com/notdodo/dodollar/block"
	"gitlab.com/notdodo/dodollar/http"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}
	http.Run(block.NewBlockchain())
}
