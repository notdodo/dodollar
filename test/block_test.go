package block_test

import (
	"bytes"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"testing"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/joho/godotenv"
	"github.com/kylelemons/godebug/pretty"
	"gitlab.com/notdodo/dodollar/block"
	bhttp "gitlab.com/notdodo/dodollar/http"
)

type target struct {
	Index      int
	Timestamp  string
	Data       []byte
	Hash       string
	HeaderHash string
	PrevHash   string
	Nonce      int64
}

var baseURL = "http://127.0.0.1:12345"
var mybc *block.Blockchain
var blockadded = 1 // Genesis il always present

func startUp() {
	mybc = block.NewBlockchain()
	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}
	bhttp.Run(mybc)
}

func TestInsert(t *testing.T) {
	go func() {
		startUp()
	}()
	var b = &target{}
	time.Sleep(5000 * time.Millisecond) // Startup server
	data := fmt.Sprintf("{\"data\": \"%s\"}", string(b64.StdEncoding.EncodeToString([]byte("create a new block"))))
	r, err := http.Post(baseURL, "application/json", bytes.NewBufferString(data))
	if err != nil {
		t.Error(err)
		t.Error("HTTP POST creation failed")
	}
	defer func() {
		if closeErr := r.Body.Close(); closeErr != nil {
			t.Error(closeErr)
		}
	}()
	if err := json.NewDecoder(r.Body).Decode(b); err != nil {
		t.Error(err)
	}
	if i, _ := strconv.Atoi(r.Status[:3]); i != 201 {
		t.Error("HTTP POST request failed: ", r.Status)
	}
	spew.Dump(b)
	blockadded += 1

	blocks, _ := mybc.GetBlocks()
	if diff := pretty.Compare(b, blocks[1]); diff != "" || len(mybc.Blocks) != 2 {
		t.Error("Blocks are not equal: ", diff)
	}
}

func TestRead(t *testing.T) {
	var b = make([]target, 2)
	r, err := http.Get(baseURL)
	if err != nil {
		t.Error("HTTP GET creation failed")
	}
	defer func() {
		if closeErr := r.Body.Close(); closeErr != nil {
			t.Error(closeErr)
		}
	}()
	if err := json.NewDecoder(r.Body).Decode(&b); err != nil {
		t.Error(err)
	}
	if i, _ := strconv.Atoi(r.Status[:3]); i != 200 {
		t.Error("HTTP GET request failed: ", r.Status)
	}
	spew.Dump(b)
	blocks, _ := mybc.GetBlocks()
	diff0 := pretty.Compare(b[0], blocks[0])
	diff1 := pretty.Compare(b[1], blocks[1])
	if diff0 != "" || diff1 != "" || len(mybc.Blocks) != blockadded {
		t.Error("Blocks are not equal: ", diff0, diff1)
	}
}

func RandStringRunes(n int) string {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func TestValidate(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < 10; i++ {
		var b = &target{}
		data := fmt.Sprintf("{\"data\": \"%s\"}", RandStringRunes(10))
		r, err := http.Post(baseURL, "application/json", bytes.NewBufferString(data))
		if err != nil {
			t.Error("HTTP POST creation failed")
		}
		defer func() {
			if closeErr := r.Body.Close(); closeErr != nil {
				t.Error(closeErr)
			}
		}()
		if err := json.NewDecoder(r.Body).Decode(b); err != nil {
			t.Error(err)
		}
		if i, _ := strconv.Atoi(r.Status[:3]); i != 201 {
			t.Error("HTTP POST request failed: ", r.Status)
		}
		blockadded += 1
	}
	if len(mybc.Blocks) != blockadded {
		t.Error(fmt.Sprintf("Blocks addition fail! Expected %d, got %d", blockadded, len(mybc.Blocks)))
	}
	// Validate the blockchain
	if err := mybc.Validate(); err != nil {
		t.Error(err)
	}
}
