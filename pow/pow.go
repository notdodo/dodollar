package pow

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"log"
	"math"
	"math/big"
	"runtime"

	"golang.org/x/crypto/sha3"
)

// first `targetZeroes` zeroes of a hash; the number of target zeroes times 4 (in bits size)
const targetZeroes = 4 * 4

var maxNonce = int64(math.MaxInt64)

type ProofOfWork struct {
	blockData       []byte
	blockHeaderHash string
	target          *big.Int
	index           int
}

/* `NewProofOfWork` initialize a BitInt with the value 1 and shift it to left of the length of the hashing algorithm minus the target length for the proof. The objective is to find a hash smaller than `target`.
`target` is a number like 0x1000000000 more zeroes less the difficulty. */
func NewProofOfWork(index int, data []byte, headerhash string) *ProofOfWork {
	target := big.NewInt(1)
	target.Lsh(target, uint(256-targetZeroes))
	proof := &ProofOfWork{data, headerhash, target, index}
	return proof
}

// IntToHex converts an int64 to a byte array
func IntToHex(num int64) []byte {
	buff := new(bytes.Buffer)
	err := binary.Write(buff, binary.BigEndian, num)
	if err != nil {
		log.Panic(err)
	}

	return buff.Bytes()
}

func (pow *ProofOfWork) prepareData(nonce int64) []byte {
	data := bytes.Join(
		[][]byte{
			[]byte(pow.blockHeaderHash),
			[]byte(pow.blockData),
			IntToHex(int64(targetZeroes)),
			IntToHex(int64(nonce)),
		},
		[]byte{},
	)
	return data
}

func (pow *ProofOfWork) Run() (int64, string) {
	nonce := make(chan int64)
	output := make(chan string)
	quit := make(chan bool)
	fmt.Printf("Mining the block %d containing \"%s\"\n", pow.index, pow.blockData)
	step := int64(math.Ceil(float64(maxNonce) / float64(runtime.NumCPU())))
	jobs := 0
	start := int64(0)
	end := step
	for jobs < runtime.NumCPU() {
		go func(start int64, end int64) {
			var hashInt big.Int
			for start < end {
				select {
				case <-quit:
					return
				default:
					hash := make([]byte, 32)
					data := pow.prepareData(start)
					sha3.ShakeSum256(hash, data)
					hashInt.SetBytes(hash)

					if hashInt.Cmp(pow.target) == -1 {
						quit <- true
						nonce <- start
						output <- hex.EncodeToString(hash)
						return
					} else {
						start++
					}
				}
			}
		}(start, end)
		start += step
		end = start + step
		jobs++
	}
	return <-nonce, <-output
}

func (pow *ProofOfWork) Validate(nonce int64) bool {
	var hashInt big.Int
	hash := make([]byte, 32)
	data := pow.prepareData(nonce)
	sha3.ShakeSum256(hash, data)
	hashInt.SetBytes(hash)
	return hashInt.Cmp(pow.target) == -1
}
