package http

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/notdodo/dodollar/block"
)

type message struct {
	Data string
}

var blockchain *block.Blockchain

// `Run` create the multiplexer
func Run(mybc *block.Blockchain) {
	blockchain = mybc
	mux := makeMuxRouter()
	httpAddr := os.Getenv("HOST")
	httpPort := os.Getenv("PORT")
	httpUrl := httpAddr + ":" + httpPort
	log.Println("Listening on: http://" + httpUrl)
	s := &http.Server{
		Addr:           httpAddr + ":" + httpPort,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	go func() {
		if err := s.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()
	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until signal is received.
	<-c

	log.Println("Shutting down")
	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	if err := s.Shutdown(ctx); err != nil {
		log.Fatal(err)
	}
}

func makeMuxRouter() http.Handler {
	muxRouter := mux.NewRouter()
	muxRouter.HandleFunc("/", handleGetBlockchain).Methods("GET")
	muxRouter.HandleFunc("/", handleWriteBlock).Methods("POST")
	return muxRouter
}

func handleGetBlockchain(w http.ResponseWriter, r *http.Request) {
	blocks, err := blockchain.GetBlocks()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	bytes, err := json.MarshalIndent(blocks, "", "\t")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if _, err := io.WriteString(w, string(bytes)); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Fatal(err)
	}
}

func handleWriteBlock(w http.ResponseWriter, r *http.Request) {
	var m message
	if err := json.NewDecoder(r.Body).Decode(&m); err != nil {
		respondWithJSON(w, r, http.StatusBadRequest, r.Body)
		return
	}
	defer func() {
		if closeErr := r.Body.Close(); closeErr != nil {
			log.Fatal(closeErr)
		}
	}()

	newBlock, err := blockchain.GenerateNextBlock([]byte(m.Data))
	if err != nil {
		log.Println(err)
		respondWithJSON(w, r, http.StatusInternalServerError, m)
		return
	}
	//spew.Dump(blockchain)
	respondWithJSON(w, r, http.StatusCreated, newBlock)
}

func respondWithJSON(w http.ResponseWriter, r *http.Request, code int, payload interface{}) {
	response, err := json.MarshalIndent(payload, "", "  ")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		if _, err := w.Write([]byte("HTTP 500: Internal Server Error")); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}
	w.WriteHeader(code)
	if _, err := w.Write(response); err != nil {
		log.Fatal(err)
	}
}
